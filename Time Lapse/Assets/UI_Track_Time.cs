﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Track_Time : MonoBehaviour
{
    public Text timeSaved;
    public GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        timeSaved = GameObject.Find("Time Saved").GetComponent<Text>();
        gameManager = GameObject.Find("Game Manager");
    }

    // Update is called once per frame
    void Update()
    {
        timeSaved.text = "" + gameManager.GetComponent<GameManager>().centuries.ToString("000") + ":" + gameManager.GetComponent<GameManager>().years.ToString("00") + ":" + gameManager.GetComponent<GameManager>().days.ToString("000") + ":" + gameManager.GetComponent<GameManager>().hours.ToString("00") + ":" + gameManager.GetComponent<GameManager>().minutes.ToString("00") + ":" + gameManager.GetComponent<GameManager>().seconds.ToString("00");
    }
}
