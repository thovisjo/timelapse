﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrolling : MonoBehaviour
{
    Vector3 temp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        temp = gameObject.transform.position;
        var d = Input.GetAxis("Mouse ScrollWheel");
        if (d < 0f && temp.y < 5.9f)
        {
            temp.y += .1f;
            gameObject.transform.position = temp;
        }
        else if (d > 0f && temp.y > -3.9f)
        {
            temp.y -= .1f;
            gameObject.transform.position = temp;
        }
    }
}
