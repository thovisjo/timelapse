﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MicroGameReceiver : MonoBehaviour
{
    public enum receiverType {Hold, LetGo, Discovery};
    public receiverType action;
    public bool cover = false;
    public int timeGoal = 0;
    public int timeNow = 0;
    public bool success = false;
    GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager");
        if (action == receiverType.Hold)
        {
            InvokeRepeating("timerBoy", 0f, 0.5f);
        }

        if (action == receiverType.LetGo)
        {
            timeGoal = 0;
        }

        if (action == receiverType.Discovery)
        {
            timeGoal = 0;
            InvokeRepeating("timerBoy", 0f, 0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if(action == receiverType.LetGo)
        {
            if(cover == true)
            {
                if (Input.GetButtonUp("Fire1"))
                {
                    success = true;
                }
            }
        }
        if(timeNow > timeGoal)
        {
            success = true;
        }
        if (success)
        {
            Invoke("returnToFuture", 2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GrabbableObject"))
        {
            cover = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GrabbableObject"))
        {
            cover = false;
        }
    }

    void timerBoy()
    {
        if (action == receiverType.Hold)
        {
            if (cover)
            {
                timeNow += 1;
            }
            else if(timeNow > 0)
            {
                timeNow -= 1;
            }
        }

        if (action == receiverType.Discovery)
        {
            if (!cover)
            {
                timeNow += 1;
            }
        }
    }
    void returnToFuture()
    {
        gameManager.GetComponent<GameManager>().targetTime = 0;
        gameManager.GetComponent<GameManager>().subtractFromCount(gameManager.GetComponent<GameManager>().moneyTime);
        SceneManager.LoadScene("TimeMachine");
    }

}
