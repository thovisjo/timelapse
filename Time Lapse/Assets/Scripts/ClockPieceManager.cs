﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockPieceManager : MonoBehaviour
{
    // Start is called before the first frame update
    public int necessary = 10;
    // Update is called once per frame
    void Update()
    {
        CheckAmountPassed();
    }

    void CheckAmountPassed()
    {
        int amount = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<ClockPiece>().passed)
            {
                amount += 1;
            }
        }

        if(amount >= necessary)
        {
            IncreaseHour();
        }
    }

    void IncreaseHour()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<ClockPiece>().passed = false;
        }
        GameObject.FindGameObjectWithTag("HourHand").GetComponent<UncontrolledClockHand>().hour += 1;
    }
}
