﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockHand : MonoBehaviour
{
    // Start is called before the first frame update
    Transform target;
    Vector3 mousepos;
    void Start()
    {
        InvokeRepeating("AddSecond", 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("ClockFace").GetComponent<ClockFace>().isActive)
        {
            target = GameObject.FindGameObjectWithTag("Mouse").transform;
            transform.right = target.position - transform.position;
        }
    }

    void AddSecond()
    {
        if (!GameObject.FindGameObjectWithTag("ClockFace").GetComponent<ClockFace>().isActive)
        {
            gameObject.transform.eulerAngles = new Vector3(0f, 0f, gameObject.transform.eulerAngles.z - 6f);
        }
    }
}
