﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UncontrolledClockHand : MonoBehaviour
{
    public int hour = -3;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.eulerAngles = new Vector3 (0f, 0f, 90f);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.eulerAngles = new Vector3(0f, 0f, hour * -30);
    }
}
