﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mouse : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isBusy = false;
    GameObject clock;
    Vector3 pz;
    Scene scene;

    public bool bookActivated = false;
    private GameObject openBook;
    private GameObject closeBook;


    private void Awake()
    {
        openBook = GameObject.Find("Journal Open");
        closeBook = GameObject.Find("Journal Closed");

        openBook.SetActive(false);
    }

    void Start()
    {
        scene = SceneManager.GetActiveScene();
        clock = GameObject.Find("Clock Face-1.png");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Book") {
            if (Input.GetMouseButtonDown(0) && bookActivated == false) {

                bookActivated = true;

                closeBook.SetActive(false);
                openBook.SetActive(true);

            }else if (Input.GetMouseButtonDown(0) && bookActivated == true)
            {
                bookActivated = false;

                closeBook.SetActive(true);
                openBook.SetActive(false);

            }
        }

        pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = 0;
        gameObject.transform.position = pz;
        if (scene.name == "TimeMachine")
        {
            isBusy = clock.GetComponent<ClockFace>().isActive;
        }
    }
}
