﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchasingButton : MonoBehaviour
{
    public Text infoText;
    public Text titleText;
    public bool isAble = false;
    public bool isVis = true;
    public int typeOfButtons = 1;
    public float cost_multiplier = 1.5f;
    public Vector4 multiplierCodeNumberCost = new Vector4(0, 0, 0, 0);
    public string title;
    GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager");
        multiplierCodeNumberCost.y = typeOfButtons;
        if(multiplierCodeNumberCost.y == 1)
        {
            title = "Stopwatch";
            multiplierCodeNumberCost.w = 100;
            multiplierCodeNumberCost.x = 1;
        }
        if (multiplierCodeNumberCost.y == 2)
        {
            title = "Wristwatch";
            multiplierCodeNumberCost.w = 1000;
            multiplierCodeNumberCost.x = 10;
        }
        if (multiplierCodeNumberCost.y == 3)
        {
            title = "Pocket Watch";
            multiplierCodeNumberCost.w = 10000;
            multiplierCodeNumberCost.x = 100;
        }
        if (multiplierCodeNumberCost.y == 4)
        {
            title = "Wall Clock";
            multiplierCodeNumberCost.w = 100000;
            multiplierCodeNumberCost.x = 500;
        }
        if (multiplierCodeNumberCost.y == 5)
        {
            title = "Grandfather Clock";
            multiplierCodeNumberCost.w = 1000000;
            multiplierCodeNumberCost.x = 1000;
        }
        if (multiplierCodeNumberCost.y == 6)
        {
            title = "Metronome";
            multiplierCodeNumberCost.w = 10000000;
            multiplierCodeNumberCost.x = 5000;
        }
        if (multiplierCodeNumberCost.y == 7)
        {
            title = "Sundial";
            multiplierCodeNumberCost.w = 100000000;
            multiplierCodeNumberCost.x = 25000;
        }
        if (multiplierCodeNumberCost.y == 8)
        {
            title = "Clocktower";
            multiplierCodeNumberCost.w = 1000000000;
            multiplierCodeNumberCost.x = 125000;
        }
        if (multiplierCodeNumberCost.y == 9)
        {
            title = "Phonebooth";
            multiplierCodeNumberCost.w = 10000000000;
            multiplierCodeNumberCost.x = 1250000;
        }
        if (multiplierCodeNumberCost.y == 10)
        {
            title = "DeLorean";
            multiplierCodeNumberCost.w = 100000000000;
            multiplierCodeNumberCost.x = 12500000;
        }
        infoText = GameObject.Find("Cost and type").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (multiplierCodeNumberCost.z > 1)
        {
            if(multiplierCodeNumberCost.y < 4)
            {
                title = title + "es";
            }
            else
            {
                title = title + "s";
            }
            
        }
        //is able to make a press of the button: presses the button sprite
        if (isAble && Input.GetButton("Fire1") && isVis)
        {
            gameObject.transform.Find("Button").GetComponent<SpriteRenderer>().enabled = false;

        }
        else
        {
            gameObject.transform.Find("Button").GetComponent<SpriteRenderer>().enabled = true;
        }

        // check if has more money time
        if (gameManager.GetComponent<GameManager>().moneyTime > multiplierCodeNumberCost.w && Input.GetButtonUp("Fire1") && isAble && isVis)
        {
            //if more moneytime and is clicked
            gameManager.GetComponent<GameManager>().subtractFromCount(Mathf.RoundToInt(multiplierCodeNumberCost.w));
            multiplierCodeNumberCost.z += 1;
            multiplierCodeNumberCost.w *= cost_multiplier;
            gameManager.GetComponent<GameManager>().timeAdded += Mathf.RoundToInt(multiplierCodeNumberCost.x);
            
        }
        if (isAble && isVis)
        {
            infoText.text = multiplierCodeNumberCost.z + "x " + " Cost: " + multiplierCodeNumberCost.w;
        }
        if (!isVis)
        {
            for(var i = 0; i < gameObject.GetComponentsInChildren<SpriteRenderer>().Length; i++)
            {
                gameObject.GetComponentsInChildren<SpriteRenderer>()[i].enabled = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Mouse"))
        {
            if (!collision.GetComponent<Mouse>().isBusy)
            {
                isAble = true;
            }
        }
        if(collision.gameObject.CompareTag("Hiding Area") && isVis)
        {
            isVis = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isAble = false;
        }
        if (collision.gameObject.CompareTag("Hiding Area") && !isVis)
        {
            {
                for (var i = 0; i < gameObject.GetComponentsInChildren<SpriteRenderer>().Length; i++)
                {
                    gameObject.GetComponentsInChildren<SpriteRenderer>()[i].enabled = true;
                }
            }
            isVis = true;
        }
    }
}

