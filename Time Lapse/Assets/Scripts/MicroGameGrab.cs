﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroGameGrab : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isGrabbable = false;
    public bool grabbed = false;
    public float distance = 1f;
    public Vector3 target;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //print(isGrabbable);
        if (isGrabbable)
        {
            if (Input.GetButton("Fire1"))
            {
                grabbed = true;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            grabbed = false;
        }

        if (grabbed)
        {
            target = GameObject.FindGameObjectWithTag("Mouse").transform.position;
            gameObject.transform.position = target;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isGrabbable = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isGrabbable = false;
        }
    }
}
