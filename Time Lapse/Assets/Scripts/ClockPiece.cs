﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockPiece : MonoBehaviour
{
    // Start is called before the first frame update
    int stage = 0;
    public bool passed = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (stage == 0)
        {
            if (collision.CompareTag("MinuteHand"))
            {
                if (!passed)
                {
                    if (GameObject.FindGameObjectWithTag("ClockFace").GetComponent<ClockFace>().isActive)
                    {
                        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().seconds += 8;
                    }
                }
                passed = true;
            }
        }
    }
}
