﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public long stage = 0;
    public long moneyTime = 0;
    public long seconds = 0;
    public long minutes = 0;
    public long hours = 0;
    public long days = 0;
    public long years = 0;
    public long decades = 0;
    public long centuries = 0;
    public long timeAdded = 1;
    public int targetTime = 0;
    public long paradoxes = 1;

    public long subseconds = 0;
    public long subminutes = 0;
    public long subhours = 0;
    public long subdays = 0;
    public long subyears = 0;
    public long subdecades = 0;
    public long subcenturies = 0;
    private static GameManager gm;

    private GameObject Book;
    private Mouse mouseScript;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (gm == null)
        {
            gm = this;
        }
        else
        {
            Object.Destroy(gameObject);
        }

    }

    private void Start()
    {
        InvokeRepeating("AddTime", 1f, 1f);
        mouseScript = GameObject.Find("Mouse").GetComponent<Mouse>();
    }

    private void Update()
    {
        
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(seconds >= 60)
        {
            minutes += seconds / 60;
            seconds = seconds % 60; //-= 60;
        }
        if(minutes >= 60)
        {
            hours += minutes/60;
            minutes = minutes % 60;
        }
        if(hours >= 24)
        {
            days += hours / 24;
            hours -= hours % 24;
        }
        if(days >= 365)
        {
            years += days / 365;
            days = days%365;
        }
        if(years >= 100)
        {
            centuries += years/100;
            years = years%100;
        }
        //moneyTime = (seconds + ((minutes + ((hours + ((days + (((decades + (centuries * 100))) * 365)) * 24)) * 60)) * 60));
        moneyTime = seconds + (minutes * 60) + (hours * 3600) + (days * 86400) + (decades * (long)3.154e+8) + (centuries * (long)3.154e+9);
        //Debug.Log(moneyTime);

        //enable book shit
        if (moneyTime > 150 && mouseScript.bookActivated == true)
        {
            Book = GameObject.Find("Journal Open");
            Book.transform.GetChild(0).gameObject.SetActive(true);

        }


        if(moneyTime > 604800)
        {
            //a week
            targetTime = 1;
        }
        if (moneyTime > 1109116800 )
        {
            //Gorbachev
            targetTime = 2;
        }
        if (moneyTime > 1781568000)
        {
            //Kennedy
            targetTime = 3;
        }
        if (moneyTime > 2358892800)
        {
            //Atomic Bomb
            targetTime = 4;
        }
        if (moneyTime > 3204921600)
        {
            //Hitler
            targetTime = 5;
        }
        if (moneyTime > 3340569600)
        {
            //Franz Ferdinand
            targetTime = 6;
        }
    }

    private void AddTime()
    {
        seconds += timeAdded * paradoxes;
        //moneyTime += 1;
        //print(timeAdded);
        //print(paradoxes);
        //print(seconds);
        //print(moneyTime);
        //print("Seconds: " + seconds + " Minutes: " + minutes + " Hours: " + hours + " Days: " + days);
    }

    private void applyMultipliers()
    {
    }

    public void subtractFromCount(long subtraction)
    {
        //subseconds = subtraction;
        //if(subseconds >= 60)
        //{
        //    subminutes = subseconds/60;
        //    subseconds -= subminutes*60;
        //}
        //if(subminutes >= 60)
        //{
        //    subhours = subminutes/60;
        //    subminutes -= subhours*60;
        //}
        //if(subhours >= 24)
        //{
        //    subdays = subhours/24;
        //    subhours -= subdays*24;
        //}
        //if(subdays >= 365)
        //{
        //    subyears = subdays/365;
        //    subdays -= subyears*365;
        //}
        //if(subyears >= 100)
        //{
        //    subcenturies = subyears/100;
        //    subyears -= subcenturies*100;
        //}
        //seconds -= subseconds;
        //minutes -= subminutes;
        //hours -= subhours;
        //days -= subdays;
        //years -= subyears;
        //decades -= subdecades;
        //centuries -= subcenturies;
        moneyTime -= subtraction;

        seconds = moneyTime; //(seconds + ((minutes + ((hours + ((days + (((decades + (centuries * 100))) * 365)) * 24)) * 60)) * 60));
        minutes = 0;
        hours = 0;
        days = 0;
        years = 0;
        decades = 0;
        centuries = 0;
    }

}
    

