﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockFace : MonoBehaviour
{
    public bool isActive = false;
    public bool isAble = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (isActive)
        {
            if (Input.GetButtonUp("Fire1"))
            {
                isActive = false;
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            if  (isAble)
            {
                isActive = true;
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isAble = true;
        }        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isAble = false;
        }
    }
}
