﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeTravel : MonoBehaviour
{
    public bool isAble = false;
    public int target_time;
    GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager");
        target_time = gameManager.GetComponent<GameManager>().targetTime;
    }

    // Update is called once per frame
    void Update()
    {
        target_time = gameManager.GetComponent<GameManager>().targetTime;
        if(isAble && Input.GetButton("Fire1"))
        {
            gameObject.GetComponentInChildren<SpriteRenderer>().enabled = false;
            Invoke("Travel", .2f);
        }
        else
        {
            gameObject.GetComponentInChildren<SpriteRenderer>().enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Mouse"))
        {
            if (!collision.GetComponent<Mouse>().isBusy)
            {
                isAble = true;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mouse"))
        {
            isAble = false;
        }
    }
    private void Travel()
    {
        gameManager.GetComponent<GameManager>().paradoxes += 1;
        gameManager.GetComponent<GameManager>().timeAdded = 1;
        if (gameManager.GetComponent<GameManager>().targetTime > 1)
        {
            SceneManager.LoadScene("BasicEvent");
        }
    }
}
